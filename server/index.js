#!/usr/bin/env node
const webserver = require("bruce-server");
const path = require("path");
const fs = require("fs");
const WebSocket = require("ws");
const wss = new WebSocket.Server({port: 5001});
const SerialPort = require("serialport");
const arduino = new SerialPort("/dev/ttyACM0", { baudRate: 115200 });

//Attempt to read the settings. If that fails, use default settings
let settings;
try {
	settings = JSON.parse(fs.readFileSync(path.join(process.env.HOME,
		".dooralarmsettings"), "utf8"));
} catch (e) {
	settings = {						//For settings that are configured by the user. The values here are the defaults.
		clock:{							//Clock related settings
			militaryTime: false,		//If true, use 24 hour clock. If false, use 12 hour clock.
			alarm: {
				hour: 0,
				minute: 0,
				enabled: [false, false, false, false, false, false, false],		//The days of the week that the alarm is enabled
				holiday: 0,				//The number of days to skip alarms. When this reaches zero, alarms are active again.
				disableWithDoor: true	//If true, you can stop the alarm by opening the door.
			}
		},
		doNotDisturb: {					//Do not distrub will dim the screen, turn off the lights, and don't sound any alarms.
			enabled: true,				//If true, do it at the scheduled times. If false, leave it off.
			start: {					//The time when lights and sound should be turned off
				hour: 21,
				minute: 0
			},
			lightsOn: {					//The time when lights should come back on. Useful if you wake up before an alarm.
				hour: 3,
				minute: 0
			},
			soundOn: {					//The time when sounds can be made again. Alarms will override this if they happen first.
				hour: 6,
				minute: 0
			}
		},
		monitoring: {
			monitorDoor: true,			//If true, monitor the door sensor
			pinEnabled: false,			//If true, door sensor settings and alert deactivation requires a PIN
			pin: "0000",				//The PIN to use
			monitorLights: true			//If true, turn off lights when room lights are off
		}
	};
	fs.writeFileSync(path.join(process.env.HOME, ".dooralarmsettings"),
		JSON.stringify(settings));
}

//Attempt to read status. If that fails, use the default
let status;
try {
	status = JSON.parse(fs.readFileSync(path.join(process.env.HOME,
		".dooralarmstatus"), "utf8"));
} catch (e) {
	status = {
		alarm: true,					//True if clock-based alarm is going off.
		alarmToday: false,				//True if an alarm has already happened and has stopped
		doorOpened: false,				//If true, the door has been opened and the door alarm has not been reset.
		lightsOn: true,					//If true, assume the lights are turned on.
		beepAllowed: true,				//True if beeps are currently allowed.
		ledOn: true						//True if the arduino's LED is on
	}
	fs.writeFileSync(path.join(process.env.HOME, ".dooralarmstatus"),
		JSON.stringify(status));
}
webserver.startHTTPServer({
	files: {
		index: "index.html",
		notFound: "404.html",
		error: "500.html",
		defext: ".html",
		root: path.join(path.dirname(__dirname), "client"),
	},
	headers: {
		"Content-Security-Policy": "default-src 'none'; script-src 'self'; connect-src 'self'; img-src 'self'; style-src 'self';"
	},
	porthttp: 5000,
	mode: "httponly",
	api: undefined
});
let arduinoBuffer = "";
let currentDate;

wss.on("connection", function(ws){
	ws.send(JSON.stringify({
		command: "settings",
		settings: settings
	}));
	ws.send(JSON.stringify({
		command: "status",
		status: status
	}));
	ws.on("message", function(message){
		let messageObj = JSON.parse(message.toString());
		switch(messageObj.command){
			case "settings": settings = messageObj.settings; break;
			case "alarmSilence":
				status.alarm = false;
				status.doorOpened = false;
				updateStatus();
			break;
			case "calOpenDoor": case "calcloseddoor": case "callighton": case "callightoff":
				arduino.write(messageObj.command + "\n");
			break;
		}
	});
});

function updateStatus(){
	wss.clients.forEach(function(ws){
		if(ws.readyState === WebSocket.OPEN){
			ws.send(JSON.stringify({
				command: "status",
				status: status
			}));
		}
	});
	if((status.alarm || status.doorOpened) && !status.doNotDisturb){ arduino.write("beepon\n")}
	else { arduino.write("beepoff"); }
}

setInterval(function(){
	let temp = arduino.read();
	if (temp != null){
		arduinoBuffer += temp;
	}
	if (arduinoBuffer.indexOf("\n") != -1){
		let temp = arduinoBuffer.split("\n");
		arduinoBuffer = "";
		for (let i = 1; i < temp.length; i++){
			arduinoBuffer += temp[i];
		}
		switch (temp[0]){
			case "open":
				status.doorOpened = true;
				updateStatus();
			break;
			case "lighton":
				status.lightsOn = true;
				updateStatus();
			break;
			case "lightoff":
				status.lightsOn = false;
				updateStatus();
			break;
		}
	}
}, 100);

setInterval(function(){
	currentDate = new Date();
	if(currentDate.getHours() == settings.alarm.hour && currentDate.getMinutes() ==
		settings.alarm.minute){
		if(!status.alarmToday){
			status.alarm = true;
			status.doNotDisturb = false;
			updateStatus();
		}
	} else {
		status.alarmToday = false;
		updateStatus();
	}
	if (settings.doNotDisturb.endabled && currentDate.getHours() +
		currentDate.getMinutes() / 60 > settings.doNotDisturb.start.hour +
		settings.doNotDisturb.start.minute / 60){
		status.lightsOn = false;
		status.beepAllowed = false;
		updateStatus();
	} else if (currentDate.getHours() + currentDate.getMinutes() / 60 >
		settings.doNotDisturb.lightsOn.hour +
		settings.doNotDisturb.lightsOn.minute / 60){
		status.lightsOn = true;
		updateStatus();
	} else if (currentDate.getHours() + currentDate.getMinutes() / 60 >
		settings.doNotDisturb.beepAllowed.hour +
		settings.doNotDisturb.beepAllowed.minute / 60){
		status.beepAllowed = true;
		updateStatus();
	}
}, 5000);
