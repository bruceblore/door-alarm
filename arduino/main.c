/* This code is an arduino sketch designed to interface with the sensors and
 * communicate the results to the Raspberry Pi (or other host machine) over USB.
 * Copy-paste into the Arduino IDE and then upload it to your board.
 *
 * Support for external LEDs, single-wire cameras that often come with cheap
 * quadcopters, buttons, and sketch update detection has been removed. Cameras
 * and user interfacing elements would be best handled by a host computer such
 * as a Raspberry Pi. I might implement automatic updating at some point.
 */

#include <EEPROM.h>

const int pinUltrasonic = 5;													//Connect to Parallax Ping))) Ultrasonic sensor.
const int pinSpeaker = 6;														//Connect to buzzer. Active high.
const int pinBoardLed = 13;														//The pin for the circuit board LED.
const int pinLight = 14;														//Connect to a photoresistor-based voltage divider.
const int eepromOffset = 0;														//This program uses 8 consecutive bytes of EEPROM. You can use this to offset the location of the first byte.

int doorCloseTime;																//The ultrasonic sensor time that was measured when the door was closed.
int doorOpenTime;																//The ultrasonic sensor time that was measured when the door was open.
int thresholdTime;																//The average of those two times. If the sample is greater, the door is open.
int darkVoltage;																//The light sensor voltage when the lights are off.
int lightVoltage;																//The light sensor voltage when the lights are on.
int thresholdVoltage;															//The average of those two voltages. If the sample is greater, the lights are on.
bool lastDoorState;																//The last door state that was sent to the host, to reduce communication.
bool lastLightState;															//The last light state that was sent to the host, to reduce communication.
String command;																	//Store a command for processing
int i;																			//Temproary values

int measureDistance(){
	int samples[5;																//Store 5 samples
	int millisStart;															//Number of milliseconds since the program has been started when the pulse was sent
	int millisEnd;																//Number of milliseconds since the program has been started when the pulse was received
	for (i=0; i<5; i++){
		pinMode(pinUltrasonic, OUTPUT);
		digitalWrite(pinUltrasonic, HIGH);
		delayMicroseconds(5);
		digitalWrite(pinUltrasonic, LOW);
		pinMode(pinUltrasonic, INPUT);
		while(!digitalRead(pinUltrasonic)){}
		millisStart = millis();
		while(digitalRead(pinUltrasonic)){}
		millisEnd = millis();
		samples[i] = millisEnd - millisStart;
		delayMicroseconds(200);
	}
	return (samples[0] + samples[1] + samples[2] + samples[3] + samples[4]) / 5;
}
void setup() {
	//Ultrasonic sensor pin not set here because it is handled by the sensor driver
	pinMode(pinSpeaker, OUTPUT);
	pinMode(pinBoardLed, OUTPUT);
	pinMode(pinLight, INPUT);
	EEPROM.get(eepromOffset, doorCloseTime);
	EEPROM.get(eepromOffset + 2, doorOpenTime);
	EEPROM.get(eepromOffset + 4, darkVoltage);
	EEPROM.get(eepromOffset + 6, lightVoltage);
	thresholdTime = (doorCloseTime + doorOpenTime) / 2;
	thresholdVoltage = (darkVoltage + lightVoltage) / 2;
	Serial.begin(115200);
	Serial.setTimeout(50);
	tone(pinSpeaker, 2000, 50);
}
void loop() {
	command = Serial.readStringUntil('\n');
	if (command == "polldoor"){
		i = measureDistance();
		if (i > thresholdTime){
			Serial.println("open");
			lastDoorState = true;
		} else {
			Serial.println("closed");
			lastDoorState = false;
		}
	}
	else if (command == "calopendoor"){
		doorOpenTime = measureDistance();
		EEPROM.put(eepromOffset + 2, doorOpenTime);
		thresholdTime = (doorCloseTime + doorOpenTime) / 2;
		Serial.println("ok");
	} else if (command == "calcloseddoor"){
		doorCloseTime = measureDistance();
		EEPROM.put(eepromOffset, doorCloseTime);
		thresholdTime = (doorCloseTime + doorOpenTime) / 2;
		Serial.println("ok");
	} else if (command == "beepon"){ tone(pinSpeaker, 5000); }
	else if (command == "beepoff"){ noTone(pinSpeaker); }
	else if (command == "ledon"){ digitalWrite(pinBoardLed, HIGH); }
	else if (command == "ledoff"){ digitalWrite(pinBoardLed, LOW); }
	else if (command == "polllight"){
		i = analogRead(pinLight);
		if (i > thresholdVoltage){
			Serial.println("light");
			lastLightState = true;
		} else {
			Serial.println("dark");
			lastLightState = false;
		}
	} else if (command == "callightoff"){
		darkVoltage = analogRead(pinLight);
		EEPROM.put(eepromOffset + 4, darkVoltage);
		thresholdVoltage = (darkVoltage + lightVoltage) / 2;
		Serial.println("ok");
	} else if (command == "callighton"){
		lightVoltage = analogRead(pinLight);
		EEPROM.put(eepromOffset + 6, lightVoltage);
		thresholdVoltage = (darkVoltage + lightVoltage) / 2;
		Serial.println("ok");
	}
	i = measureDistance();
	if(i > thresholdTime && !lastDoorState){
		Serial.println("open");
		lastDoorState = true;
	} else if (i <= thresholdTime && lastDoorState){
		Serial.println("closed");
		lastDoorState = false;
	}
	i = analogRead(pinLight);
	if(i > thresholdVoltage && !lastLightState){
		Serial.println("light");
		lastLightState = true;
	} else if (i <= thresholdVoltage && lastLightState){
		Serial.println("dark");
		lastLightState = false;
	}
}
