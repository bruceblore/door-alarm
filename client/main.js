import { stat } from "fs";

let settings = {						//For settings that are configured by the user. The values here are the defaults and will be used until the configuration is loaded from the derver
	clock:{								//Clock related settings
		militaryTime: false,			//If true, use 24 hour clock. If false, use 12 hour clock.
		alarm: {
			hour: 0,
			minute: 0,
			enabled: [false, false, false, false, false, false, false],			//The days of the week that the alarm is enabled
			holiday: 0,					//The number of days to skip alarms. When this reaches zero, alarms are active again.
			disableWithDoor: true		//If true, you can stop the alarm by opening the door.
		}
	},
	doNotDisturb: {						//Do not distrub will turn off the led on the arduino and don't sound any door alarms.
		enabled: true,					//If true, do it at the scheduled times. If false, leave it off.
		start: {						//The time when lights and sound should be turned off
			hour: 21,
			minute: 0
		},
		lightsOn: {						//The time when lights should come back on. Useful if you wake up before an alarm.
			hour: 3,
			minute: 0
		},
		soundOn: {						//The time when sounds can be made again. Alarms will override this if they happen first.
			hour: 6,
			minute: 0
		}
	},
	monitoring: {
		monitorDoor: true,				//If true, monitor the door sensor
		pinEnabled: false,				//If true, door sensor settings and alert deactivation requires a PIN
		pin: "0000",					//The PIN to use
		monitorLights: true				//If true, turn off lights when room lights are off
	}
};
let currentStatus = {
	alarm: false,						//True if clock-based alarm is going off.
	alarmToday: false,					//True if an alarm has already happened and has stopped
	doorOpened: false,					//If true, the door has been opened and the door alarm has not been reset.
	lightsOn: true,						//If true, assume the lights are turned on.
	beepAllowed: true,					//True if beeps are currently allowed.
	ledOn: true							//True if the arduino's LED is on
}
let currentDate;						//Store the current date shown on the screen.

/**
 * This function takes a raw hour on a 24 hour clock and returns the hour on a
 * 12 or 24 hour clock by user preference.
 *
 * @param rawHour the hour on a 24 hour clock
 * @return The hour on the users preferred clock style
 */
function getPreferredHour(rawHour){
	if (settings.clock.militaryTime){ return rawHour; }
	else if (rawHour == 0){ return 12; }
	else if (rawHour < 12){ return rawHour; }
	else { return rawHour - 12; }
}
/**
 * This function takes an hour on a 24 hour clock and returns the AM or PM
 * string, or nothing if the user prefers a 24 hour clock.
 *
 * @param rawHour the hour on a 24 hour clock
 * @return the AM/PM string
 */
function getAmPm(rawHour){
	if (settings.clock.militaryTime){ return ""; }
	else { return rawHour < 12 ? " AM" : " PM"; }
}
/**
 * Makes sure one-digit numbers have a leading zero, return two digit numbers as-is
 * @param number the number to give a leading zero
 * @return the number with a leading zero if one digit
 */
function leadingZero(number){
	return number > 9 ? number : '0' + number.toString();
}
/**
 * Updates the clock display. It is recommended to call this every second.
 */
function updateClock(){
	currentDate = new Date();
	document.getElementById("clock-time").innerText = `${getPreferredHour(currentDate.getHours())}:${leadingZero(currentDate.getMinutes())}:${leadingZero(currentDate.getSeconds())}${getAmPm(currentDate.getHours())}`;
	document.getElementById("clock-info-date").innerText = `${["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"][currentDate.getDay()]}, ${["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"][currentDate.getMonth()]} ${currentDate.getDate()}, ${currentDate.getFullYear()}`;
	let alarmText;
	if (settings.clock.alarm.holiday) { alarmText == "holiday"; }
	else if (settings.clock.alarm.enabled[currentDate.getDay()] &&
		currentDate.getHours() + currentDate.getMinutes() / 60 <
		settings.clock.alarm.hour + settings.clock.alarm.minute / 60){
		alarmText = `${getPreferredHour(settings.clock.alarm.hour)}:${leadingZero(settings.clock.alarm.minute)}${getAmPm(settings.clock.alarm.hour)}`;
	} else if (settings.clock.alarm.enabled[currentDate.getDay() + 1] &&
		currentDate.getHours() + currentDate.getMinutes() / 60 >
		settings.clock.alarm.hour + settings.clock.alarm.minute / 60){
		alarmText = `${getPreferredHour(settings.clock.alarm.hour)}:${leadingZero(settings.clock.alarm.minute)}${getAmPm(settings.clock.alarm.hour)}
		`;
	} else if (settings.clock.alarm.enabled.indexOf(true) != -1){
		alarmText = ""
	} else { alarmText = "off"; }
	document.getElementById("clock-info-alarm").innerText = alarmText;
}
document.addEventListener("click", ()=>{ document.body.requestFullscreen(); });
document.addEventListener("keypress", ()=>{ document.body.requestFullscreen(); });

const socket = new WebSocket("ws://localhost:5001");
socket.onopen = function(){
	setInterval(updateClock, 1000);		//Start running the clock
}
socket.onmessage = function(message){
	let messageObj = JSON.parse(message);
	switch (messageObj.command){
		case "settings": settings = messageObj.settings; break;
		case "status":
			currentStatus = messageObj.status;
			if (currentStatus.alarm){
				document.getElementById("top-bar").style.color = "var(--topbar-color-alarm";
				document.getElementById("top-bar").innerText = "Alarm (tap to stop)";
			} else if (currentStatus.alarm){
				document.getElementById("top-bar").style.color = "var(--topbar-color-alarm";
				document.getElementById("top-bar").innerText = "Door opened (tap to silence)";
			} else if (settings.monitoring.monitorDoor){
				document.getElementById("top-bar").style.color = "var(--topbar-color-ready";
				document.getElementById("top-bar").innerText = "Monitoring door";
			} else {
				document.getElementById("top-bar").style.color = "var(--topbar-color-notification";
				document.getElementById("top-bar").innerText = "Idle";
			}
			if (settings.monitoring.monitorLights && currentStatus.lightsOn){
				document.getElementsByTagName("style")[0].href = "styles-dark.css";
			} else {
				document.getElementsByTagName("style")[0].href = "styles-light.css";
			}
		break;
	}
}
socket.onerror = function(){ location.reload(); }

let enteredPin = "";
document.getElementById("key-0").addEventListener("click", function(event){
	enteredPin += '0';
});
document.getElementById("key-1").addEventListener("click", function(event){
	enteredPin += '1';
});
document.getElementById("key-2").addEventListener("click", function(event){
	enteredPin += '2';
});
document.getElementById("key-3").addEventListener("click", function(event){
	enteredPin += '3';
});
document.getElementById("key-4").addEventListener("click", function(event){
	enteredPin += '4';
});
document.getElementById("key-5").addEventListener("click", function(event){
	enteredPin += '5';
});
document.getElementById("key-6").addEventListener("click", function(event){
	enteredPin += '6';
});
document.getElementById("key-7").addEventListener("click", function(event){
	enteredPin += '7';
});
document.getElementById("key-8").addEventListener("click", function(event){
	enteredPin += '8';
});
document.getElementById("key-9").addEventListener("click", function(event){
	enteredPin += '9';
});
document.getElementById("key-clear").addEventListener("click", function(event){
	enteredPin = "";
});

function askForPin(callback){
	if (settings.monitoring.pinEnabled){
		document.getElementById("pin-entry").style.display = "grid";
		enteredPin = "";
		document.getElementById("key-enter").addEventListener("click", function(event){
			enteredPin += '0';
		});
		let old_element = document.getElementById("key-enter");
		let new_element = old_element.cloneNode(true);
		old_element.parentNode.replaceChild(new_element, old_element);
		new_element.addEventListener("click", ()=>{
			if (enteredPin == settings.monitoring.pin){ callback(); }
			else {
				alert("WRONG");
				enteredPin = "";
			}
		});
	} else { callback(); }
}

document.getElementById("top-bar").addEventListener("click", function(even){
	if (!currentStatus.doorOpened{
		socket.send("alarmSilence");
	} else {askForPin(() => { socket.send("alarmSilence"); }); }
})
